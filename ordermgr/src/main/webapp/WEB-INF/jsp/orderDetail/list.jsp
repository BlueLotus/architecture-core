<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<title>List Order Detail</title>
		<link href="${pageContext.request.contextPath}/static/css/application.css" rel="stylesheet">
		<script src="${pageContext.request.contextPath}/static/js/application.js"></script>
		<script src="${pageContext.request.contextPath}/static/js/jquery-1.11.0.js"></script>
	</head>
	<body>
		<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
		<%@taglib prefix="myTag" tagdir="/WEB-INF/tags"%>
	
		<table width="100%" border="1" cellpadding="0" cellspacing="1" class="tableLine DoubleColorTable">
			<tr>
				<td colspan="6">
					<a href="${pageContext.request.contextPath}/orderDetailservice/query">Go to query</a>
					&nbsp;&nbsp;
					<a href="${pageContext.request.contextPath}/orderDetailservice/add">Go to create</a>
				</td>
			</tr>
			<tr>
				<td colspan=${6+1} align="center" class="tableLineBg">Order Detail List</td>
			</tr>
			<tr>
				<td>Order UID</td>
				<td>Item UID</td>
				<td>Purchase quantity</td>
				<td>Item price</td>
				<td>Total price</td>
				<td>Discount price</td>
				<td>Operation</td>
			</tr>
			<c:forEach var="orderDetail" items="${page.result}">
				<tr>
					<td>${orderDetail.orderUuid}</td>
					<td>${orderDetail.goodsUuid}</td>
					<td>${orderDetail.orderNum}</td>
					<td>${orderDetail.price}</td>
					<td>${orderDetail.money}</td>
					<td>${orderDetail.saveMoney}</td>
					<td>
					<a href="${pageContext.request.contextPath}/orderDetailservice/update/${orderDetail.uuid}">Modify</a> |
					<a href="${pageContext.request.contextPath}/orderDetailservice/delete/${orderDetail.uuid}">Delete</a>
					</td>
				</tr>
			</c:forEach>
			<tr>
				<td colspan=${6+1} align="center">
					<input type="hidden" id="jsonQueryString" value='${orderDetailWebModel.jsonQueryString}' />
					<myTag:page page="${page}"></myTag:page>
				</td>
			</tr>
		</table>
	</body>
</html>