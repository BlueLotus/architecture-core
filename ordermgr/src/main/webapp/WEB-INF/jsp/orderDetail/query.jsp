<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<title>Query for OrderDetail</title>
		<link href="${pageContext.request.contextPath}/static/css/application.css" rel="stylesheet">
		<script src="${pageContext.request.contextPath}/static/js/jquery-1.11.0.js"></script>
	</head>
	<body>
		<script type="text/javascript">
			$().ready(function() {
						$("#btn_query").click(function() {
								var json = '{"orderUuid":"' + $("#orderUuid").val() + '"' + 
										   ',"goodsUuid":"' + $("#goodsUuid").val() + '"' + 
										   ',"orderNum":"' + $("#orderNum").val() + '"' + 
										   ',"price":"' + $("#price").val() + '"' + 
										   ',"money":"' + $("#money").val() + '"' + 
										   ',"saveMoney":"' + $("#saveMoney").val() + '"' + '}';
										window.location.href = "${pageContext.request.contextPath}/orderDetailservice/list?jsonQueryString=" + json;
											});
						});
		</script>
	
		<table width="100%" border="1" cellpadding="0" cellspacing="1" class="tableLine">
			<tr>
				<td colspan=4 align=center class="tableLineBg">OrderDetail Query</td>
			</tr>
			<tr>
				<td>Order UID</td>
				<td><input type="text" id="orderUuid" name="orderUuid" class="input"></td>
				<td>Item UID</td>
				<td><input type="text" id="goodsUuid" name="goodsUuid" class="input"></td>
			</tr>
			<tr>
				<td>Purchase quantity</td>
				<td><input type="text" id="orderNum" name="orderNum" class="input"></td>
				<td>Item price</td>
				<td><input type="text" id="price" name="price" class="input"></td>
				</tr>
			<tr>
				<td>Total price</td>
				<td><input type="text" id="money" name="money" class="input"></td>
				<td>Discount price</td>
				<td><input type="text" id="saveMoney" name="saveMoney" class="input"></td>
			</tr>
			<tr>
				<td colspan=4 align=center><input id="btn_query" type="button" value="query" class="button"></td>
			</tr>
		</table>
	</body>
</html>