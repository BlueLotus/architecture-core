<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<title>Query for Order</title>
		<link href="${pageContext.request.contextPath}/static/css/application.css" rel="stylesheet">
		<script src="${pageContext.request.contextPath}/static/js/jquery-1.11.0.js"></script>
	</head>
	<body>
		<script type="text/javascript">
			$().ready(function() {
						$("#btn_query").click(function() {
								var json = '{"customerUuid":"' + $("#customerUuid").val() + '"' + 
										   ',"orderTime":"' + $("#orderTime").val() + '"' + 
										   ',"totalMoney":"' + $("#totalMoney").val() + '"' + 
										   ',"saveMoney":"' + $("#saveMoney").val() + '"' + 
										   ',"state":"' + $("#state").val() + '"' + '}';
										window.location.href = "${pageContext.request.contextPath}/orderservice/list?jsonQueryString=" + json;
											});
						});
		</script>
	
		<table width="100%" border="1" cellpadding="0" cellspacing="1" class="tableLine">
			<tr>
				<td colspan=4 align=center class="tableLineBg">Order Query</td>
			</tr>
			<tr>
				<td>Customer UID</td>
				<td><input type="text" id="customerUuid" name="customerUuid" class="input"></td>
				<td>Order creation date</td>
				<td><input type="text" id="orderTime" name="orderTime" class="input"></td>
			</tr>
			<tr>
				<td>Total price</td>
				<td><input type="text" id="totalMoney" name="totalMoney" class="input"></td>
				<td>Discount price</td>
				<td><input type="text" id="saveMoney" name="saveMoney" class="input"></td>
			</tr>
			<tr>
				<td>State</td>
				<td><input type="text" id="state" name="state" class="input">
			</td>
			<tr>
				<td colspan=4 align=center><input id="btn_query" type="button" value="query" class="button"></td>
			</tr>
		</table>
	</body>
</html>