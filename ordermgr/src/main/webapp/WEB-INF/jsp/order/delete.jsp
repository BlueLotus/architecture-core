<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<title>Delete Order</title>
		<link href="${pageContext.request.contextPath}/static/css/application.css" rel="stylesheet">
	</head>
	<body>
		<form action="${pageContext.request.contextPath}/orderservice/delete" method="post">
			<input type="hidden" name="uuid" value="${order.uuid}" /> 
			<table width="100%" border="1" cellpadding="0" cellspacing="1" class="tableLine">
				<tr>
					<td colspan=4 align=center class="tableLineBg">Delete Order</td>
				</tr>
				<tr>
					<td>Customer UID</td>
					<td><input type="text" name="customerUuid" value="${order.customerUuid}" class="input"></td>
					<td>Order creation date</td>
					<td><input type="text" name="orderTime"  value="${order.orderTime}" class="input"></td>
				</tr>
				<tr>
					<td>Total price</td>
					<td><input type="text" name="totalMoney" value="${order.totalMoney}" class="input"></td>
					<td>Discount price</td>
					<td><input type="text" name="saveMoney"  value="${order.saveMoney}" class="input"></td>
				</tr>
				<tr>
					<td>State</td>
					<td><input type="text" name="state" value="${order.state}" class="input"></td>
				</tr>
				<tr>
					<td colspan=4 align=center><input type="submit" value="delete" class="button"></td>
				</tr>
			</table>
		</form>
	</body>
</html>