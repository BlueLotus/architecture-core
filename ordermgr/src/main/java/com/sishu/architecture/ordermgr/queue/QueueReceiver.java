package com.sishu.architecture.ordermgr.queue;

import javax.jms.ConnectionFactory;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.sishu.architecture.cartmgr.service.CartService;
import com.sishu.architecture.ordermgr.service.OrderDetailService;
import com.sishu.architecture.ordermgr.service.OrderService;
import com.sishu.architecture.ordermgr.thread.OrderThread;
import com.sishu.architecture.storemgr.service.StoreService;
import com.sishu.util.jms.ConnectionConstantList;


/**
 * 
 * @author Carl Adler(C.A.)
 *
 */
@Service
public class QueueReceiver implements ServletContextListener {
	
	@Autowired 
	private CartService cartService;
	
	@Autowired
	private OrderService orderService;
	
	@Autowired
	private OrderDetailService orderDetailService;
	
	@Autowired
	private StoreService storeService;
	
	@Override
	public void contextInitialized(ServletContextEvent event) {
		ApplicationContext ctx = WebApplicationContextUtils.getWebApplicationContext(event.getServletContext());
		QueueReceiver queueReceiver = (QueueReceiver) ctx.getBean("queueReceiver");
		queueReceiver.acceptMessage();
	}
	
	@Override
	public void contextDestroyed(ServletContextEvent event) {
		
	}

	public void acceptMessage() {
		ConnectionFactory connectionFactory = new ActiveMQConnectionFactory(ConnectionConstantList.WIRELESS_BROKER1);
		for(int i = 0; i < 1; i++) {
			 new OrderThread(cartService, orderService, orderDetailService, storeService, connectionFactory).start();
		}
	}
	
}
