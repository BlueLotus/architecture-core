package com.sishu.architecture.ordermgr.queue;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.apache.activemq.ActiveMQConnectionFactory;

import com.sishu.util.jms.ConnectionConstantList;


/**
 * 
 * @author Carl Adler(C.A.)
 *
 */
public class QueueSender {
	
	private static ConnectionFactory connectionFactory = new ActiveMQConnectionFactory(ConnectionConstantList.WIRELESS_BROKER1);
	
	public static void sendMessage(int customerUuid) {
		
		Connection connection = null;
		Session session = null;
		
		try {
			connection = connectionFactory.createConnection();
			connection.start();
			
			session = connection.createSession(Boolean.TRUE, Session.CLIENT_ACKNOWLEDGE);
			
			Destination destination = session.createQueue("order-queue");
			MessageProducer producer = session.createProducer(destination);
			
			TextMessage message = session.createTextMessage(String.valueOf(customerUuid));
			producer.send(message);
			
			session.commit();
		} catch (JMSException e) {
			e.printStackTrace();
		} finally {
			try {
				session.close();
			} catch (JMSException e) {
				e.printStackTrace();
			}
			try {
				connection.close();
			} catch (JMSException e) {
				e.printStackTrace();
			}
		}
		
	}

}
