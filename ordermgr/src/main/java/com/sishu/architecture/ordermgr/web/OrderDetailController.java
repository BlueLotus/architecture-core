package com.sishu.architecture.ordermgr.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.sishu.architecture.ordermgr.domain.OrderDetail;
import com.sishu.architecture.ordermgr.domain.OrderDetailQueryModel;
import com.sishu.architecture.ordermgr.domain.OrderDetailWebModel;
import com.sishu.architecture.ordermgr.service.OrderDetailService;
import com.sishu.pageutil.Page;
import com.sishu.util.json.JsonHelper;

/**
 * 
 * @author Carl Adler(C.A.)
 *
 * Generated by X-gen
 *
 */
@Controller
@RequestMapping("/orderDetailservice")
public class OrderDetailController {

	@Autowired
	private OrderDetailService orderDetailService;
	
	@RequestMapping(value = "add", method = RequestMethod.GET)
	public String addForm() {
		return "orderDetail/add";
	}
	
	@RequestMapping(value = "add", method = RequestMethod.POST)
	public String add(@ModelAttribute("orderDetail") OrderDetail orderDetail) {
		orderDetailService.create(orderDetail);
		return "orderDetail/success";
	}
	
	@RequestMapping(value = "update/{uuid}", method = RequestMethod.GET)
	public String updateForm(Model uiModel, @PathVariable("uuid") int uuid) {
		OrderDetail orderDetail = orderDetailService.getByUuid(uuid);
		uiModel.addAttribute("orderDetail", orderDetail);
		return "orderDetail/update";
	}
	
	@RequestMapping(value = "update", method = RequestMethod.POST)
	public String update(@ModelAttribute("orderDetail") OrderDetail orderDetail) {
		orderDetailService.update(orderDetail);
		return "orderDetail/success";
	}
	
	@RequestMapping(value = "delete/{uuid}", method = RequestMethod.GET)
	public String deleteForm(Model uiModel, @PathVariable("uuid") int uuid) {
		OrderDetail orderDetail = orderDetailService.getByUuid(uuid);
		uiModel.addAttribute("orderDetail", orderDetail);
		return "orderDetail/delete";
	}
	
	@RequestMapping(value = "delete", method = RequestMethod.POST)
	public String delete(@RequestParam("uuid") int uuid) {
		orderDetailService.delete(uuid);
		return "orderDetail/success";
	}
	
	@RequestMapping(value = "list", method = RequestMethod.GET)
	public String listOrderDetail(@ModelAttribute("orderDetailWebModel") OrderDetailWebModel orderDetailWebModel, Model uiModel) {
			
		OrderDetailQueryModel orderDetailQueryModel = null;
		
		String jsonQueryString = orderDetailWebModel.getJsonQueryString();
		
		if(jsonQueryString == null || jsonQueryString.trim().length() == 0) {
			orderDetailQueryModel = new OrderDetailQueryModel();
		} else {
			jsonQueryString = jsonQueryString.replace("-", "%");
			orderDetailQueryModel = (OrderDetailQueryModel) JsonHelper.str2Object(jsonQueryString, OrderDetailQueryModel.class);
		}
		
		orderDetailQueryModel.getPage().setNowPage(orderDetailWebModel.getNowPage());
		
		if(orderDetailWebModel.getPageShow() > 0) {
			orderDetailQueryModel.getPage().setPageShow(orderDetailWebModel.getPageShow());
		}
		
		Page dbPage = orderDetailService.getByConditionPage(orderDetailQueryModel);
		
		uiModel.addAttribute("orderDetailWebModel", orderDetailWebModel);
		uiModel.addAttribute("page", dbPage);
		
		return "orderDetail/list";
	}
	
	@RequestMapping(value = "query", method = RequestMethod.GET)
	public String queryForm() {
		return "orderDetail/query";
	}
	
}
