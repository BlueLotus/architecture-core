package com.sishu.architecture.ordermgr.thread;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageListener;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.springframework.beans.factory.annotation.Autowired;

import com.sishu.architecture.cartmgr.domain.Cart;
import com.sishu.architecture.cartmgr.domain.CartQueryModel;
import com.sishu.architecture.cartmgr.service.CartService;
import com.sishu.architecture.ordermgr.domain.Order;
import com.sishu.architecture.ordermgr.domain.OrderDetail;
import com.sishu.architecture.ordermgr.domain.OrderQueryModel;
import com.sishu.architecture.ordermgr.service.OrderDetailService;
import com.sishu.architecture.ordermgr.service.OrderService;
import com.sishu.architecture.storemgr.domain.Store;
import com.sishu.architecture.storemgr.service.StoreService;
import com.sishu.pageutil.Page;
import com.sishu.util.format.DateFormatHelper;

/**
 * 
 * @author Carl Adler(C.A.)
 *
 */
public class OrderThread extends Thread {
	
	private static final int PAGE_SHOW = 10;
	private static final float UNIT_PRICE = 100.0f;
	
	@Autowired 
	private CartService cartService;
	
	@Autowired
	private OrderService orderService;
	
	@Autowired
	private OrderDetailService orderDetailService;
	
	@Autowired
	private StoreService storeService;
	
	ConnectionFactory connectionFactory;

	public OrderThread(CartService cartService, OrderService orderService,
			OrderDetailService orderDetailService, StoreService storeService,
			ConnectionFactory connectionFactory) {
		super();
		this.cartService = cartService;
		this.orderService = orderService;
		this.orderDetailService = orderDetailService;
		this.storeService = storeService;
		this.connectionFactory = connectionFactory;
	}
	
	public void run() {
		Connection connection = null;
		final Session session;
		try {
			connection = connectionFactory.createConnection();
			connection.start();
			
			session = connection.createSession(Boolean.TRUE, Session.AUTO_ACKNOWLEDGE);
			
			Destination destination = session.createQueue("order-queue");
			MessageConsumer consumer = session.createConsumer(destination);
			
			consumer.setMessageListener(new MessageListener() {
				
				@Override
				public void onMessage(Message message) {
					
					try {
						TextMessage textMessage = (TextMessage) message;
						int customerUuid = Integer.parseInt(textMessage.getText());
						
						session.commit();
						
						Page<Cart> cartContent = null;
						CartQueryModel cartQueryModel = new CartQueryModel();
						cartQueryModel.getPage().setPageShow(PAGE_SHOW);
						cartQueryModel.setCustomerUuid(customerUuid);
						cartContent = cartService.getByConditionPage(cartQueryModel);

						float totalMoney = 0.0f;

						for (Cart cart : cartContent.getResult()) {
							totalMoney += UNIT_PRICE;
						}

						Order order = new Order();
						order.setCustomerUuid(customerUuid);
						order.setOrderTime(DateFormatHelper.long2Str(System.currentTimeMillis()));
						order.setSaveMoney(0.0f);
						order.setTotalMoney(totalMoney);
						order.setState(1);

						orderService.create(order);

						OrderQueryModel orderQueryModel = new OrderQueryModel();
						orderQueryModel.setOrderTime(order.getOrderTime());
						Page<Order> orderPage = orderService.getByConditionPage(orderQueryModel);
						order = orderPage.getResult().get(0);

						for (Cart cart : cartContent.getResult()) {
							OrderDetail orderDetail = new OrderDetail();
							orderDetail.setGoodsUuid(cart.getGoodsUuid());
							orderDetail.setOrderNum(cart.getBuyNum());
							orderDetail.setPrice(UNIT_PRICE);
							orderDetail.setMoney(orderDetail.getPrice() * orderDetail.getOrderNum());
							orderDetail.setSaveMoney(0.0f);
							orderDetail.setOrderUuid(order.getUuid());
							
							orderDetailService.create(orderDetail);
							
							synchronized (storeService) {
								Store store = storeService.getByGoodsUuid(cart.getGoodsUuid());
								store.setStoreNum(store.getStoreNum() - orderDetail.getOrderNum());
								storeService.update(store);
							}

							//cartService.delete(cart.getUuid());
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			});
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
}