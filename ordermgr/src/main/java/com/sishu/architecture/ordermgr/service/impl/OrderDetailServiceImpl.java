package com.sishu.architecture.ordermgr.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sishu.architecture.common.service.impl.GeneralServiceImpl;
import com.sishu.architecture.ordermgr.dao.OrderDetailDAO;
import com.sishu.architecture.ordermgr.domain.OrderDetail;
import com.sishu.architecture.ordermgr.domain.OrderDetailQueryModel;
import com.sishu.architecture.ordermgr.service.OrderDetailService;

/**
 * 
 * @author Carl Adler(C.A.)
 *
 * Generated by X-gen
 *
 */
@Service
@Transactional
public class OrderDetailServiceImpl extends GeneralServiceImpl<OrderDetail,OrderDetailQueryModel> implements OrderDetailService {
	
	private OrderDetailDAO dao;
	
	@Autowired
	public void setDao(OrderDetailDAO dao) {
		this.dao = dao;
		super.setGeneralDAO(dao);
	}
	
}
