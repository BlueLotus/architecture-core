   function turnPage(nowPage) {
    	var urls = window.location.href;
    	var site = urls.indexOf("?"); 
    	if(site > 0){
    		urls = urls.substring(0,site);
    	}
    	urls = urls+"?nowPage="+nowPage;
    	
    	var jsonQueryString = document.getElementById("jsonQueryString").value;
    	if(jsonQueryString != null && jsonQueryString != ''){
    		urls = urls + "&jsonQueryString=" + jsonQueryString;
    	}
    	window.location.href = urls;
    }
