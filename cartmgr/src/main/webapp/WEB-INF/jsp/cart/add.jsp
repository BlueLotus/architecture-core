<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title>Create Cart</title>
	<link href="${pageContext.request.contextPath}/static/css/application.css" rel="stylesheet">
	</head>
	<body>
		<form action="${pageContext.request.contextPath}/cartservice/add" method="post">
			<table width="100%" border="1" cellpadding="0" cellspacing="1" class="tableLine">
				<tr>
					<td colspan=4 align=center class="tableLineBg">Cart Creation</td>
				</tr>
				<tr>
					<td>Customer UID</td>
					<td><input type="text" name="customerUuid" class="input"></td>
					<td>Item UID</td>
					<td><input type="text" name="goodsUuid" class="input"></td>
				</tr>
				<tr>
					<td>Item quantity</td>
					<td><input type="text" name="buyNum" class="input"></td>
				</tr>
				<tr>
					<td colspan=4 align=center><input type="submit" value="create" class="button"></td>
				</tr>
			</table>
		</form>
	</body>
</html>