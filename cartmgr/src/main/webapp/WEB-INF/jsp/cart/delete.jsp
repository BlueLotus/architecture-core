<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title>Delete Cart</title>
	<link href="${pageContext.request.contextPath}/static/css/application.css" rel="stylesheet">
	</head>
	<body>
		<form action="${pageContext.request.contextPath}/cartservice/delete" method="post">
			<input type="hidden" name="uuid" value="${cart.uuid}" />
			<table width="100%" border="1" cellpadding="0" cellspacing="1" class="tableLine">
				<tr>
					<td colspan=4 align=center class="tableLineBg">Delete Cart</td>
				</tr>
				<tr>
					<td>Customer UID</td>
					<td><input type="text" name="cart.customerUuid" value="${cart.customerUuid}" class="input"></td>
					<td>Item UID</td>
					<td><input type="text" name="cart.goodsUuid" value="${cart.goodsUuid}" class="input"></td>
				</tr>
				<tr>
					<td>Item quantity</td>
					<td><input type="text" name="cart.buyNum" value="${cart.buyNum}" class="input"></td>
				</tr>
				<tr>
					<td colspan=4 align=center><input type="submit" value="delete" class="button"></td>
				</tr>
			</table>
		</form>
	</body>
</html>