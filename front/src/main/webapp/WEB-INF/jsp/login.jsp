<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<title>Login Page</title>
	</head>
	<body>
		<form action="${pageContext.request.contextPath}/login" method="post">
			<table width="100%">
				<tr height="200">
					<td colspan="2">&nbsp;</td>
				</tr>
				<tr>
					<td width="300">&nbsp;</td>
					<td>
						<table>
							<tr>
								<td>Login Name:</td>
								<td><input type="text" name="customerId"></td>
							</tr>
							<tr>
								<td>Password:</td>
								<td><input type="password" name="password"></td>
							</tr>
							<tr>
								<td colspan="2"><input type="submit" value="Login"></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</form>
	</body>
</html>