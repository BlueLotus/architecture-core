<%@tag pageEncoding="UTF-8" description="Pagination"%>
<%@ attribute name="page" type="com.sishu.pageutil.Page" required="true" description="Pagination Object"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<table width="100%" align="center">
	<tr>
		<td>
			<span class="page-info">
				[Total record: ${page.totalCount} / for ${page.totalPage} pages, current page: ${page.nowPage}]
			</span>
		</td>
	</tr>
	<tr>
		<td>
			<c:choose>
				<c:when test="${page.nowPage==1}">
                	Home Page &nbsp;&nbsp;
                	prev&nbsp;&nbsp;
            	</c:when>
				<c:otherwise>
					<a href="#" onclick="turnPage(1);" title="Home Page">Home Page</a>&nbsp;&nbsp;
                	<a href="#" onclick="turnPage(${page.nowPage - 1});" title="prev">prev</a>&nbsp;&nbsp;
            	</c:otherwise>
			</c:choose> 
			<c:choose>
				<c:when test="${page.nowPage==page.totalPage}">
                	next&nbsp;&nbsp;
                	last&nbsp;&nbsp;
            	</c:when>
				<c:otherwise>
					<a href="#" onclick="turnPage(${page.nowPage + 1});" title="next">next</a>&nbsp;&nbsp;
                	<a href="#" onclick="turnPage(${page.totalPage});" title="last">last</a>&nbsp;&nbsp;
           		</c:otherwise>
			</c:choose>
		</td>
	</tr>
</table>