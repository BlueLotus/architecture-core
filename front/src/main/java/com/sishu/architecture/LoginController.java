package com.sishu.architecture;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.sishu.architecture.customermgr.domain.Customer;
import com.sishu.architecture.customermgr.service.CustomerService;

/**
 * 
 * @author Carl Adler(C.A.)
 *
 */
@Controller
@RequestMapping("/")
public class LoginController {

	@Autowired
	private CustomerService customerService;
	
	@RequestMapping(value = "/toLogin", method = RequestMethod.GET)
	public String loginForm() {
		return "login";
	}
	
	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public String login(@RequestParam("customerId") String customerId,
						@RequestParam("password") String password, HttpServletResponse response) {
		
		if(customerId == null || customerId.trim().length() == 0) return "login";
		
		Customer customer = customerService.getByCustomerId(customerId);
		
		if(customer == null || customer.getUuid() <= 0) return "login";
		
		Cookie cookie = new Cookie("customerLogin", customer.getUuid() + "," + System.currentTimeMillis());
		response.addCookie(cookie);
		
		return "redirect:/index";
	}
	
}
