package com.sishu.architecture;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.sishu.architecture.cartmgr.domain.Cart;
import com.sishu.architecture.cartmgr.domain.CartQueryModel;
import com.sishu.architecture.cartmgr.service.CartService;
import com.sishu.architecture.goodsmgr.domain.Goods;
import com.sishu.architecture.goodsmgr.domain.GoodsQueryModel;
import com.sishu.architecture.goodsmgr.service.GoodsService;
import com.sishu.architecture.ordermgr.service.OrderDetailService;
import com.sishu.architecture.ordermgr.service.OrderService;
import com.sishu.architecture.storemgr.service.StoreService;
import com.sishu.pageutil.Page;

/**
 * 
 * @author Carl Adler(C.A.)
 *
 */
@Controller
@RequestMapping("/")
public class IndexController {
	
	private static final int PAGE_SHOW = 10;
	
	@Autowired
	private GoodsService goodsService;
	
	@Autowired
	private CartService cartService;
	
	@Autowired
	private OrderService orderService;
	
	@Autowired
	private OrderDetailService orderDetailService;
	
	@Autowired
	private StoreService storeService;
	
	@RequestMapping(value = "/index", method = RequestMethod.GET)
	public String toIndex(Model uiModel) {
		GoodsQueryModel goodsQueryModel = new GoodsQueryModel();
		goodsQueryModel.getPage().setPageShow(100);
		Page<Goods> page = goodsService.getByConditionPage(goodsQueryModel);
		uiModel.addAttribute("page", page);
		return "index";
	}
	
	@RequestMapping(value = "/itemdesc/{goodsUuid}", method = RequestMethod.GET)
	public String itemDesc(Model uiModel, @PathVariable("goodsUuid") int goodsUuid) {
		Goods goods = goodsService.getByUuid(goodsUuid);
		uiModel.addAttribute("item", goods);
		return "goods/desc";
	}
	
	@RequestMapping(value = "/mycart", method = RequestMethod.GET)
	public String toCart(Model uiModel, @CookieValue("customerLogin") String customerLogin) {
		Page<Cart> page = obtainCustomerShoppingCart(customerLogin);
		uiModel.addAttribute("page", page);
		return "cart/mycart";
	}
	
	@RequestMapping(value = "/addtocart/{goodsUuid}", method = RequestMethod.GET)
	public String addToCart(Model uiModel, @PathVariable("goodsUuid") int goodsUuid, @CookieValue("customerLogin") String customerLogin) {
		int customerUuid = Integer.parseInt(customerLogin.split(",")[0]);
		Cart cart = new Cart();
		cart.setBuyNum(1);
		cart.setCustomerUuid(customerUuid);
		cart.setGoodsUuid(goodsUuid);
		cartService.create(cart);
		Page<Cart> page = obtainCustomerShoppingCart(customerLogin);
		uiModel.addAttribute("page", page);
		return "cart/mycart";
	}
	
	@RequestMapping(value = "/order", method = RequestMethod.GET)
	public String order(@CookieValue("customerLogin") String customerLogin) {
		int customerUuid = Integer.parseInt(customerLogin.split(",")[0]);
		orderService.order(customerUuid);
		return "success";
	}
	
	private Page<Cart> obtainCustomerShoppingCart(String customerLogin) {
		Page<Cart> result = null;
		int customerUuid = Integer.parseInt(customerLogin.split(",")[0]);
		CartQueryModel cartQueryModel = new CartQueryModel();
		cartQueryModel.getPage().setPageShow(PAGE_SHOW);
		cartQueryModel.setCustomerUuid(customerUuid);
		result = cartService.getByConditionPage(cartQueryModel);
		return result;
	}
	
}
