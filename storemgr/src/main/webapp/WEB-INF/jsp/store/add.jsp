<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<title>Create Store</title>
		<link href="${pageContext.request.contextPath}/static/css/application.css" rel="stylesheet">
	</head>
	<body>
		<form action="${pageContext.request.contextPath}/storeservice/add" method="post">
			<table width="100%" border="1" cellpadding="0" cellspacing="1" class="tableLine">
				<tr>
					<td colspan=4 align=center class="tableLineBg">Store Creation</td>
				</tr>
	
				<tr>
					<td>Item UID</td>
					<td><input type="text" name="goodsUuid" class="input"></td>
					<td>In stock</td>
					<td><input type="text" name="storeNum" class="input"></td>
				</tr>
				<tr>
					<td colspan=4 align=center><input type="submit" value="create" class="button"></td>
				</tr>
			</table>
		</form>
	</body>
</html>