package com.sishu.util.json;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * 
 * @author Carl Adler(C.A.)
 *
 */
public class JsonHelper {

	private JsonHelper() {
	}
	
	public static String object2Str(Object obj) {
		String resultString = "";
		ObjectMapper mapper = new ObjectMapper();
		
		try {
			resultString = mapper.writeValueAsString(obj);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		
		return resultString;
	}
	
	public static Object str2Object(String str, Class cls) {
		Object resultObject = null;
		ObjectMapper mapper = new ObjectMapper();
		
		try {
			resultObject = mapper.readValue(str, cls);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return resultObject;
	}
	
}
