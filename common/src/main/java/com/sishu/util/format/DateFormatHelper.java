package com.sishu.util.format;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 
 * @author Carl Adler(C.A.)
 *
 */
public class DateFormatHelper {
	
	private DateFormatHelper(){}
	
	private final static String FORMAT_PATTERN = "yyyy-MM-dd HH:mm:ss";
	
	public static String long2Str(long time) {
		Date date = new Date(time);
		DateFormat dateFormat = new SimpleDateFormat(FORMAT_PATTERN);
		return dateFormat.format(date);
	}
	
	public static long str2Long(String dateString) {
		long result = 0;
		DateFormat dateFormat = new SimpleDateFormat(FORMAT_PATTERN);
		try {
			result = dateFormat.parse(dateString).getTime();
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return result;
	}
	
}
