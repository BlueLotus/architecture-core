package com.sishu.memcached;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.GenericXmlApplicationContext;
import org.springframework.stereotype.Service;

import com.danga.MemCached.MemCachedClient;

/**
 * 
 * @author Carl Adler(C.A.)
 *
 */
@Service
public class ClientTestWithSpring {
	
	@Autowired
	private MemCachedClient client;
	
	public static void main(String[] args) {
		
		GenericXmlApplicationContext ctx = new GenericXmlApplicationContext("classpath:applicationContext.xml");
		
		ClientTestWithSpring clientTest = (ClientTestWithSpring) ctx.getBean("clientTestWithSpring");
		
		clientTest.client.set("key1", "11123456");
		clientTest.client.set("key2", "12");
		
		Object object = clientTest.client.get("key1");
		Object object2 = clientTest.client.get("key2");
		
		System.out.println("object: " + object);
		System.out.println("object2: " + object2);
		
	}

}
