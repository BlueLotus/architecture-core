package com.sishu.memcached;

import com.danga.MemCached.MemCachedClient;
import com.danga.MemCached.SockIOPool;

/**
 * 
 * @author Carl Adler(C.A.)
 *
 */
public class CacheHelper {
	
	private static MemCachedClient memCachedClient = new MemCachedClient();
	
	private CacheHelper() {}
	
	static {
		String[] servers = {"192.168.1.106:2222", "192.168.1.106:2223"};
		Integer[] weights = {1, 1};
		SockIOPool pool = SockIOPool.getInstance();
		pool.setServers(servers);
		pool.setWeights(weights);
		pool.setInitConn(5);
		pool.setMinConn(5);
		pool.setMaxConn(250);
		pool.setMaxIdle(1000 * 60 * 60 *6);
		pool.setMaintSleep(30);
		
		pool.setNagle(false);
		pool.setSocketConnectTO(0);
		pool.setSocketTO(3000);
		pool.setHashingAlg(3);
		pool.initialize();
	}
	
	public static MemCachedClient getMemCachedClient() {
		return memCachedClient;
	}

}
