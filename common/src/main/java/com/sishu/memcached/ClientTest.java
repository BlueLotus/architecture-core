package com.sishu.memcached;

import java.util.ArrayList;
import java.util.List;

import com.danga.MemCached.MemCachedClient;

/**
 * 
 * @author Carl Adler(C.A.)
 *
 */
public class ClientTest {
	
	public static void main(String[] args) {
		MemCachedClient client = CacheHelper.getMemCachedClient();
		
		client.add("data1", "12345");
		Object object = client.get("data1");
		
		UserModel userModel = new UserModel("1", "Carl", 25);
		client.set("user1", userModel);
		Object object2 = client.get("user1");
		
		List<UserModel> list = new ArrayList<UserModel>();
		UserModel userModel2 = new UserModel("2", "Miffy", 20);
		UserModel userModel3 = new UserModel("3", "RuRu", 23);
		list.add(userModel2);
		list.add(userModel3);
		client.set("list1", list);
		Object object3 = client.get("list1");
		
		
		System.out.println("Obj: " + object);
		System.out.println("Obj2: " + object2);
		System.out.println("Obj3: " + object3);
	}

}
