package com.sishu.architecture.common.dao;

import java.util.List;

/**
 * 
 * @author Carl Adler(C.A.)
 *
 */
public interface GeneralDAO<MODEL, QUERYMODEL> {
	
	public void create(MODEL model);
	
	public void update(MODEL model);
	
	public void delete(int uuid);
	
	public MODEL getByUuid(int uuid);
	
	public List<MODEL> getByConditionPage(QUERYMODEL queryModel);
	
}
