package com.sishu.architecture.common.service;

import com.sishu.architecture.common.domain.GeneralModel;
import com.sishu.pageutil.Page;

/**
 * 
 * @author Carl Adler(C.A.)
 *
 */
public interface GeneralService<MODEL, QUERYMODEL extends GeneralModel> {
	
	public void create(MODEL model);
	
	public void update(MODEL model);
	
	public void delete(int uuid);
	
	public MODEL getByUuid(int uuid);
	
	public Page<MODEL> getByConditionPage(QUERYMODEL queryModel);

}
