package com.sishu.architecture.common.service.impl;

import java.util.List;

import com.sishu.architecture.common.dao.GeneralDAO;
import com.sishu.architecture.common.domain.GeneralModel;
import com.sishu.architecture.common.service.GeneralService;
import com.sishu.pageutil.Page;

/**
 * 
 * @author Carl Adler(C.A.)
 *
 */
public class GeneralServiceImpl<MODEL, QUERYMODEL extends GeneralModel> implements GeneralService<MODEL, QUERYMODEL> {
	
	private GeneralDAO<MODEL, QUERYMODEL> generalDAO = null;
	
	public void setGeneralDAO(GeneralDAO<MODEL, QUERYMODEL> generalDAO) {
		this.generalDAO = generalDAO;
	}

	@Override
	public void create(MODEL model) {
		generalDAO.create(model);
	}

	@Override
	public void update(MODEL model) {
		generalDAO.update(model);
	}

	@Override
	public void delete(int uuid) {
		generalDAO.delete(uuid);
	}

	@Override
	public MODEL getByUuid(int uuid) {
		return generalDAO.getByUuid(uuid);
	}

	@Override
	public Page<MODEL> getByConditionPage(QUERYMODEL queryModel) {
		List<MODEL> list = generalDAO.getByConditionPage(queryModel);
		queryModel.getPage().setResult(list);
		return queryModel.getPage();
	}

}
