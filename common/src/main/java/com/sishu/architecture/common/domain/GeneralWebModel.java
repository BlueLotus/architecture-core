package com.sishu.architecture.common.domain;

/**
 * 
 * @author Carl Adler(C.A.)
 *
 */
public class GeneralWebModel {
	
	private String jsonQueryString = "";
	private int nowPage = 1;
	private int pageShow = 0;
	
	public String getJsonQueryString() {
		return jsonQueryString;
	}
	public void setJsonQueryString(String jsonQueryString) {
		this.jsonQueryString = jsonQueryString;
	}
	
	public int getNowPage() {
		return nowPage;
	}
	public void setNowPage(int nowPage) {
		this.nowPage = nowPage;
	}
	
	public int getPageShow() {
		return pageShow;
	}
	public void setPageShow(int pageShow) {
		this.pageShow = pageShow;
	}
	
	@Override
	public String toString() {
		return "GeneralWebModel [jsonQueryString=" + jsonQueryString
				+ ", nowPage=" + nowPage + ", pageShow=" + pageShow + "]";
	}

}
