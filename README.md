This project is a simple business web site with basic CRUD functionality, 

and the purpose is to test the following features:


1. High extensibility
2. High performance
3. Big data
4. High concurrency
5. distributed system


For more information, please read the source code and document.