package com.sishu.architecture.goodsmgr.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import com.danga.MemCached.MemCachedClient;
import com.sishu.architecture.goodsmgr.domain.Goods;
import com.sishu.architecture.goodsmgr.domain.GoodsQueryModel;
import com.sishu.pageutil.Page;

/**
 * 
 * @author Carl Adler(C.A.)
 *
 */
@Service
@Primary
public class GoodsMemcachedDAO implements GoodsDAO {
	
	private final String CACHE_PREFIX = "Goods";
	
	@Autowired
	private MemCachedClient memcachedClient;
	
	@Autowired
	private GoodsMongoDAO dao;

	@Override
	public void create(Goods model) {
		dao.create(model);
	}

	@Override
	public void update(Goods model) {
		dao.update(model);
		//Check the data exist in memcached or not and update memcached if the data exist.
		String cacheId = CACHE_PREFIX + model.getUuid();
		Object object = memcachedClient.get(cacheId);
		if(object != null) {
			memcachedClient.set(cacheId, model);
		}
	}

	@Override
	public void delete(int uuid) {
		dao.delete(uuid);
		//Check the data exist in memcached or not and delete it from memcached if exist.
		String cacheId = CACHE_PREFIX + uuid;
		Object object = memcachedClient.get(cacheId);
		if(object != null) {
			memcachedClient.delete(cacheId);
		}
	}

	@Override
	public Goods getByUuid(int uuid) {
		Goods goods = null;
		//1. Query from memcached, if exist, obtain from cache.
		String cacheId = CACHE_PREFIX + uuid;
		Object object = memcachedClient.get(cacheId);
		if(object != null) {
			goods = (Goods) object;
			return goods;
		}
		//2. Else, obtain from database.
		goods = dao.getByUuid(uuid);
		//3. Add the data into memcached.
		memcachedClient.set(cacheId, goods);
		return goods;
	}

	@Override
	public List<Goods> getByConditionPage(GoodsQueryModel queryModel) {
		/*//1. Query all the ids according to the conditions.
		List<Integer> ids = dao.getIdsByConditionPage(queryModel);
		//2. Find all the corresponding objects in memcached.
		List<Goods> goodsListFromMemCached = new ArrayList<Goods>();
		
		List<Integer> idsNotFound = new ArrayList<Integer>();
		
		for(Integer id : ids) {
			Object object = memcachedClient.get(CACHE_PREFIX + id);
			if(object != null) {
				Goods goods = (Goods) object;
				goodsListFromMemCached.add(goods);
			} else {
				idsNotFound.add(id);
			}
		}
		System.out.println("ID not found: " + idsNotFound);
		//3. Obtain objects from database if not found in memcached, then add into memcached.
		if(idsNotFound.size() > 0) {
			for(Integer id : idsNotFound) {
				Goods goods = dao.getByUuid(id);
				memcachedClient.set(CACHE_PREFIX + id, goods);
				goodsListFromMemCached.add(goods);
			}
		}*/

		return null;
	}

	@Override
	public Page<Goods> getByCondition(GoodsQueryModel goodsQueryModel) {
		return dao.getByCondition(goodsQueryModel);
	}

}
