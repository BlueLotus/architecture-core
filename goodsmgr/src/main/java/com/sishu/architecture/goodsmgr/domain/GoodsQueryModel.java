package com.sishu.architecture.goodsmgr.domain;

/**
 * 
 * @author Carl Adler(C.A.)
 *
 * Generated by X-gen
 *
 */
public class GoodsQueryModel extends Goods {

	public String toString() {
		return "Model"+this.getClass().getName()+","+super.toString()+" ,[]";
	}

}