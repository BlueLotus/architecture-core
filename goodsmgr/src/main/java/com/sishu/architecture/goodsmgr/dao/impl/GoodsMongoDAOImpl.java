package com.sishu.architecture.goodsmgr.dao.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.sishu.architecture.goodsmgr.dao.GoodsMongoDAO;
import com.sishu.architecture.goodsmgr.domain.Goods;
import com.sishu.architecture.goodsmgr.domain.GoodsQueryModel;
import com.sishu.pageutil.Page;

/**
 * 
 * @author Carl Adler(C.A.)
 *
 */
@Service
public class GoodsMongoDAOImpl implements GoodsMongoDAO {
	
	@Autowired
	private MongoTemplate mongoTemplate;
	
	private final String COLLECTION_NAME = "goods";
	
	@Override
	public void create(Goods model) {
		mongoTemplate.insert(model, COLLECTION_NAME);
	}

	@Override
	public void update(Goods model) {
		DBCollection goods = mongoTemplate.getCollection(COLLECTION_NAME);
				
		goods.update(new BasicDBObject("uuid", model.getUuid()), 
				new BasicDBObject("uuid", model.getUuid())
					.append("name", model.getName())
					.append("imgPath", model.getImgPath())
					.append("description", model.getDescription()));
	}

	@Override
	public void delete(int uuid) {
		Criteria criteria = new Criteria("uuid").is(uuid);
		mongoTemplate.remove(new Query(criteria), COLLECTION_NAME);
	}

	@Override
	public Goods getByUuid(int uuid) {
		Criteria criteria = new Criteria("uuid").is(uuid);
		List<Goods> goods = mongoTemplate.find(new Query(criteria), Goods.class, COLLECTION_NAME);
		if(goods != null && goods.size() > 0)
			return goods.get(0);
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Page<Goods> getByCondition(GoodsQueryModel goodsQueryModel) {
		Criteria criteria = new Criteria();
		
		Integer uuid = goodsQueryModel.getUuid();
		String name = goodsQueryModel.getName();
		String desc = goodsQueryModel.getDescription();
		
		if(uuid != null && uuid > 0) {
			criteria.andOperator(new Criteria("uuid").is(uuid));
		} else if(name != null && name.trim().length() > 0) {
			criteria.andOperator(new Criteria("name").regex(name));
		} else if(desc != null && desc.trim().length() > 0) {
			criteria.andOperator(new Criteria("description").regex(desc));
		}
		
		int totalCount = (int) mongoTemplate.count(new Query(criteria), COLLECTION_NAME);
		
		Page<Goods> page = goodsQueryModel.getPage();
		page.setTotalCount(totalCount);
		List<Goods> goodsList = mongoTemplate.find(new Query(criteria).skip(page.getStart()).limit(page.getPageShow()), Goods.class, COLLECTION_NAME);
		page.setResult(goodsList);
		
		return page;
	}
	
	@Override
	public List<Goods> getByConditionPage(GoodsQueryModel queryModel) {
		return null;
	}

	@Override
	public List<Goods> getByIds(String ids) {
		List<Integer> uuids = new ArrayList<Integer>();
		String[] idList = ids.split(",");
		
		for (String id : idList) {
			uuids.add(Integer.parseInt(id));
		}
		
		Criteria criteria = new Criteria("uuid").in(uuids);
		
		List<Goods> goods = mongoTemplate.find(new Query(criteria), Goods.class, COLLECTION_NAME);
		if(goods != null && goods.size() > 0) {
			return goods;
		}
		return null;
	}

	@Override
	public List<Integer> getIdsByConditionPage(GoodsQueryModel queryModel) {
		return null;
	}

}
