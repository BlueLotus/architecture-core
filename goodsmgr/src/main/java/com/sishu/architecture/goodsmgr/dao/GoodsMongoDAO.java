package com.sishu.architecture.goodsmgr.dao;

import java.util.List;

import com.sishu.architecture.goodsmgr.domain.Goods;
import com.sishu.architecture.goodsmgr.domain.GoodsQueryModel;

/**
 * 
 * @author Carl Adler(C.A.)
 *
 */
public interface GoodsMongoDAO extends GoodsDAO {
	
	public List<Goods> getByIds(String ids);

	public List<Integer> getIdsByConditionPage(GoodsQueryModel queryModel);

}
