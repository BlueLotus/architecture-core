package com.sishu.architecture.goodsmgr.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.sishu.architecture.goodsmgr.domain.Goods;
import com.sishu.architecture.goodsmgr.domain.GoodsQueryModel;

/**
 * 
 * @author Carl Adler(C.A.)
 *
 */
//@Repository
public interface GoodsMapper extends GoodsDAO{
	
	public List<Goods> getByIds(String ids);

	public List<Integer> getIdsByConditionPage(GoodsQueryModel queryModel);
	
}
