package com.sishu.architecture.goodsmgr.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.GenericXmlApplicationContext;
import org.springframework.stereotype.Service;

import com.sishu.architecture.goodsmgr.domain.Goods;
import com.sishu.architecture.goodsmgr.domain.GoodsQueryModel;
import com.sishu.pageutil.Page;

/**
 * 
 * @author Carl Adler(C.A.)
 *
 */
@Service
public class ClientTestForMemcached {
	
	@Autowired
	private GoodsDAO goodsDAO;
	
	public static void main(String[] args) {
		
		GenericXmlApplicationContext ctx = new GenericXmlApplicationContext("classpath:applicationContext.xml");
		
		ClientTestForMemcached clientTest = (ClientTestForMemcached) ctx.getBean("clientTestForMemcached");
		
		/*Goods goods = clientTest.goodsDAO.getByUuid(2);
		System.out.println(goods);*/
		
		GoodsQueryModel goodsQueryModel = new GoodsQueryModel();
		goodsQueryModel.getPage().setPageShow(100);
		
		List<Goods> list = clientTest.goodsDAO.getByConditionPage(goodsQueryModel);
		System.out.println("list: " + list);
		
		
	}

}
