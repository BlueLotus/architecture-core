<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<title>Update Customer</title>
		<link href="${pageContext.request.contextPath}/static/css/application.css" rel="stylesheet">
	</head>
	<body>
		<form action="${pageContext.request.contextPath}/customerservice/update" method="post">
			<input type="hidden" name="uuid" value="${customer.uuid}" />
			<input type="hidden" name="registerTime" value="${customer.registerTime }" />
			<table width="100%" border="1" cellpadding="0" cellspacing="1" class="tableLine">
				<tr>
					<td colspan=4 align=center class="tableLineBg">Customer Modification</td>
				</tr>
				<tr>
					<td>Customer ID</td>
					<td><input type="text" name="customerId" value="${customer.customerId}" class="input"></td>
					<td>Password</td>
					<td><input type="text" name="password" value="${customer.password}" class="input"></td>
				</tr>
				<tr>
					<td>Display Name</td>
					<td><input type="text" name="showName" value="${customer.showName}" class="input"></td>
					<td>Real Name</td>
					<td><input type="text" name="trueName" value="${customer.trueName}" class="input"></td>
				</tr>
				<tr>
					<td colspan=4 align=center><input type="submit" value="modify" class="button"></td>
				</tr>
			</table>
		</form>
	</body>
</html>