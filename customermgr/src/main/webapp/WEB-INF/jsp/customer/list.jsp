<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<title>List Customer</title>
		<link href="${pageContext.request.contextPath}/static/css/application.css" rel="stylesheet">
		<script src="${pageContext.request.contextPath}/static/js/application.js"></script>
		<script src="${pageContext.request.contextPath}/static/js/jquery-1.11.0.js"></script>
	</head>
	<body>
		<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
		<%@taglib prefix="myTag" tagdir="/WEB-INF/tags"%>
	
		<table width="100%" border="1" cellpadding="0" cellspacing="1" class="tableLine DoubleColorTable">
			<tr>
				<td colspan="6">
					<a href="${pageContext.request.contextPath}/customerservice/query">Go to query</a>
					&nbsp;&nbsp; 
					<a href="${pageContext.request.contextPath}/customerservice/add">Go to create</a>
				</td>
			</tr>
			<tr>
				<td colspan="6" align="center" class="tableLineBg">Customer List</td>
			</tr>
			<tr>
				<td>Customer ID</td>
				<td>Display Name</td>
				<td>Real Name</td>
				<td>Password</td>
				<td>Register Time</td>
				<td>Operation</td>
			</tr>
			<c:forEach var="customer" items="${page.result}">
				<tr>
					<td>${customer.customerId}</td>
					<td>${customer.showName}</td>
					<td>${customer.trueName}</td>
					<td>${customer.password}</td>
					<td>${customer.registerTime}</td>
					<td>
						<a href="${pageContext.request.contextPath}/customerservice/update/${customer.uuid}">Modify</a>	|	
						<a href="${pageContext.request.contextPath}/customerservice/delete/${customer.uuid}">Delete</a>
					</td>
				</tr>
			</c:forEach>
			<tr>
				<td colspan="6" align="center">
					<input type="hidden" id="jsonQueryString" value='${customerWebModel.jsonQueryString}' />
					<myTag:page page="${page}"></myTag:page>
				</td>
			</tr>
		</table>
	</body>
</html>