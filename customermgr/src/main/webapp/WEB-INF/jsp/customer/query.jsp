<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<title>Query for Customers</title>
		<link href="${pageContext.request.contextPath}/static/css/application.css" rel="stylesheet">
		<script src="${pageContext.request.contextPath}/static/js/jquery-1.11.0.js"></script>
	</head>
	<body>
		<script type="text/javascript">
			$().ready(function() {
				$("#btn_query").click(function() {
									var json = '{"customerId":"' + $("#customerId").val() + '"'
										+ ',"password":"' + $("#password").val() + '"'
										+ ',"showName":"' + $("#showName").val() + '"' 
										+ ',"trueName":"' + $("#trueName").val() + '"' + '}';
										window.location.href = 
											"${pageContext.request.contextPath}/customerservice/list?jsonQueryString=" + json;
											});
						});
		</script>
	
		<table width="100%" border="1" cellpadding="0" cellspacing="1" class="tableLine">
			<tr>
				<td colspan=4 align=center class="tableLineBg">Customer Query</td>
			</tr>
			<tr>
				<td>Customer ID</td>
				<td><input type="text" id="customerId" name="customerId" class="input"></td>
				<td>Password</td>
				<td><input type="text" id="password" name="password" class="input"></td>
			</tr>
			<tr>
				<td>Display Name</td>
				<td><input type="text" id="showName" name="showName" class="input"></td>
				<td>Real Name</td>
				<td><input type="text" id="trueName" name="trueName" class="input"></td>
			</tr>
			<tr>
				<td colspan=4 align=center><input id="btn_query" type="button" value="query" class="button"></td>
			</tr>
		</table>
	</body>
</html>