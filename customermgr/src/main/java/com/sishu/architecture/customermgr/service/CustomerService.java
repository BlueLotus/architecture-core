package com.sishu.architecture.customermgr.service;

import com.sishu.architecture.common.service.GeneralService;
import com.sishu.architecture.customermgr.domain.Customer;
import com.sishu.architecture.customermgr.domain.CustomerQueryModel;

/**
 * 
 * @author Carl Adler(C.A.)
 *
 */
public interface CustomerService extends GeneralService<Customer, CustomerQueryModel> {

	public Customer getByCustomerId(String customerId);
	
}
