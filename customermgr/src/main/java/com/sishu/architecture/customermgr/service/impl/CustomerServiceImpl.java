package com.sishu.architecture.customermgr.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sishu.architecture.common.service.impl.GeneralServiceImpl;
import com.sishu.architecture.customermgr.dao.CustomerDAO;
import com.sishu.architecture.customermgr.domain.Customer;
import com.sishu.architecture.customermgr.domain.CustomerQueryModel;
import com.sishu.architecture.customermgr.service.CustomerService;

/**
 * 
 * @author Carl Adler(C.A.)
 *
 */
@Service
@Transactional
public class CustomerServiceImpl extends GeneralServiceImpl<Customer, CustomerQueryModel> implements CustomerService {
	
	private CustomerDAO customerDAO;
	
	@Autowired
	public void setCustomerDAO(CustomerDAO customerDAO) {
		this.customerDAO = customerDAO;
		super.setGeneralDAO(customerDAO);
	}

	@Override
	public Customer getByCustomerId(String customerId) {
		return customerDAO.getByCustomerId(customerId);
	}
	
}
