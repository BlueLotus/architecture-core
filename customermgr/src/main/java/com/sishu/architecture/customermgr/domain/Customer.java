package com.sishu.architecture.customermgr.domain;

import com.sishu.architecture.common.domain.GeneralModel;

/**
 * 
 * @author Carl Adler(C.A.)
 *
 */
public class Customer extends GeneralModel {

	private static final long serialVersionUID = 905277060801872450L;
	
	private String customerId;
	private String password;
	private String showName;
	private String trueName;
	private String registerTime;
	
	public String getCustomerId() {
		return customerId;
	}
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}
	
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	public String getShowName() {
		return showName;
	}
	public void setShowName(String showName) {
		this.showName = showName;
	}
	
	public String getTrueName() {
		return trueName;
	}
	public void setTrueName(String trueName) {
		this.trueName = trueName;
	}
	
	public String getRegisterTime() {
		return registerTime;
	}
	public void setRegisterTime(String registerTime) {
		this.registerTime = registerTime;
	}
	
	@Override
	public String toString() {
		return "Customer [uuid=" + getUuid() + ", customerId=" + customerId
				+ ", password=" + password + ", showName=" + showName
				+ ", trueName=" + trueName + ", registerTime=" + registerTime
				+ "]";
	}

}
