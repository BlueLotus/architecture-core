package com.sishu.architecture.customermgr.dao;

import org.springframework.stereotype.Repository;

import com.sishu.architecture.common.dao.GeneralDAO;
import com.sishu.architecture.customermgr.domain.Customer;
import com.sishu.architecture.customermgr.domain.CustomerQueryModel;

/**
 * 
 * @author Carl Adler(C.A.)
 *
 */
@Repository
public interface CustomerDAO extends GeneralDAO<Customer, CustomerQueryModel>{
	
	public Customer getByCustomerId(String customerId);

}
