package com.sishu.architecture.customermgr.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.sishu.architecture.customermgr.domain.Customer;
import com.sishu.architecture.customermgr.domain.CustomerQueryModel;
import com.sishu.architecture.customermgr.domain.CustomerWebModel;
import com.sishu.architecture.customermgr.service.CustomerService;
import com.sishu.pageutil.Page;
import com.sishu.util.format.DateFormatHelper;
import com.sishu.util.json.JsonHelper;

/**
 * 
 * @author Carl Adler(C.A.)
 *
 */
@Controller
@RequestMapping(value = "/customerservice")
public class CustomerController {
	
	@Autowired
	private CustomerService customerService;
	
	@RequestMapping(value = "add", method = RequestMethod.GET)
	public String addForm() {
		return "customer/add";
	}
	
	@RequestMapping(value = "add", method = RequestMethod.POST)
	public String add(@ModelAttribute("customer") Customer customer) {
		customer.setRegisterTime(DateFormatHelper.long2Str(System.currentTimeMillis()));
		customerService.create(customer);
		return "customer/success";
	}
	
	@RequestMapping(value = "update/{customerUuid}", method = RequestMethod.GET)
	public String updateForm(Model uiModel, @PathVariable("customerUuid") int customerUuid) {
		Customer customer = customerService.getByUuid(customerUuid);
		uiModel.addAttribute("customer", customer);
		return "customer/update";
	}
	
	@RequestMapping(value = "update", method = RequestMethod.POST)
	public String update(@ModelAttribute("customer") Customer customer) {
		customerService.update(customer);
		return "customer/success";
	}
	
	@RequestMapping(value = "delete/{customerUuid}", method = RequestMethod.GET)
	public String deleteForm(Model uiModel, @PathVariable("customerUuid") int customerUuid) {
		Customer customer = customerService.getByUuid(customerUuid);
		uiModel.addAttribute("customer", customer);
		return "customer/delete";
	}
	
	@RequestMapping(value = "delete", method = RequestMethod.POST)
	public String delete(@RequestParam("uuid") int customerUuid) {
		customerService.delete(customerUuid);
		return "customer/success";
	}
	
	@RequestMapping(value = "list", method = RequestMethod.GET)
	public String listCustomer(@ModelAttribute("customerWebModel") CustomerWebModel customerWebModel, Model uiModel) {
			
		CustomerQueryModel customerQueryModel = null;
		
		String jsonQueryString = customerWebModel.getJsonQueryString();
		
		if(jsonQueryString == null || jsonQueryString.trim().length() == 0) {
			customerQueryModel = new CustomerQueryModel();
		} else {
			customerQueryModel = (CustomerQueryModel) JsonHelper.str2Object(jsonQueryString, CustomerQueryModel.class);
		}
		
		customerQueryModel.getPage().setNowPage(customerWebModel.getNowPage());
		
		if(customerWebModel.getPageShow() > 0) {
			customerQueryModel.getPage().setPageShow(customerWebModel.getPageShow());
		}
		
		Page dbPage = customerService.getByConditionPage(customerQueryModel);
		
		uiModel.addAttribute("customerWebModel", customerWebModel);
		uiModel.addAttribute("page", dbPage);
		
		return "customer/list";
	}
	
	@RequestMapping(value = "query", method = RequestMethod.GET)
	public String queryForm() {
		return "customer/query";
	}
	
}
