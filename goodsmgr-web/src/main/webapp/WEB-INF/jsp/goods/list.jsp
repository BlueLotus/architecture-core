<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title>List Goods</title>
	<link href="${pageContext.request.contextPath}/static/css/application.css" rel="stylesheet">
	<script src="${pageContext.request.contextPath}/static/js/application.js"></script>
	<script src="${pageContext.request.contextPath}/static/js/jquery-1.11.0.js"></script>
</head>
<body>
	<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
	<%@taglib prefix="myTag" tagdir="/WEB-INF/tags"%>

	<table width="100%" border="1" cellpadding="0" cellspacing="1" class="tableLine DoubleColorTable">
		<tr>
			<td colspan="6">
				<a href="${pageContext.request.contextPath}/goodsservice/query">Go to query</a> 
				&nbsp;&nbsp; 
				<a href="${pageContext.request.contextPath}/goodsservice/add">Go to create</a>
			</td>
		</tr>
		<tr>
			<td colspan="4" align="center" class="tableLineBg">Item List</td>
		</tr>
		<tr>
			<td>Item name</td>
			<td>IMG path</td>
			<td>Item desc</td>
			<td>Operation</td>
		</tr>
		<c:forEach var="goods" items="${page.result}">
			<tr>
				<td>${goods.name}</td>
				<td>${goods.imgPath}</td>
				<td>${goods.description}</td>
				<td>
					<a href="${pageContext.request.contextPath}/goodsservice/update/${goods.uuid}">Modify</a>	|	
					<a href="${pageContext.request.contextPath}/goodsservice/delete/${goods.uuid}">Delete</a>
				</td>
			</tr>
		</c:forEach>
		<tr>
			<td colspan="4" align="center">
			<input type="hidden" id="jsonQueryString" value='${goodsWebModel.jsonQueryString}' />
			<myTag:page page="${page}"></myTag:page></td>
		</tr>
	</table>
</body>
</html>