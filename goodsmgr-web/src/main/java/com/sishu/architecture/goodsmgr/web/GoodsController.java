package com.sishu.architecture.goodsmgr.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.sishu.architecture.goodsmgr.domain.Goods;
import com.sishu.architecture.goodsmgr.domain.GoodsQueryModel;
import com.sishu.architecture.goodsmgr.domain.GoodsWebModel;
import com.sishu.architecture.goodsmgr.service.GoodsService;
import com.sishu.pageutil.Page;
import com.sishu.util.json.JsonHelper;

/**
 * 
 * @author Carl Adler(C.A.)
 *
 * Generated by X-gen
 *
 */
@Controller
@RequestMapping("/goodsservice")
public class GoodsController {

	@Autowired
	private GoodsService goodsService;
	
	@RequestMapping(value = "add", method = RequestMethod.GET)
	public String addForm() {
		return "goods/add";
	}
	
	@RequestMapping(value = "add", method = RequestMethod.POST)
	public String add(@ModelAttribute("goods") Goods goods) {
		goodsService.create(goods);
		return "goods/success";
	}
	
	@RequestMapping(value = "update/{uuid}", method = RequestMethod.GET)
	public String updateForm(Model uiModel, @PathVariable("uuid") int uuid) {
		Goods goods = goodsService.getByUuid(uuid);
		uiModel.addAttribute("goods", goods);
		return "goods/update";
	}
	
	@RequestMapping(value = "update", method = RequestMethod.POST)
	public String update(@ModelAttribute("goods") Goods goods) {
		goodsService.update(goods);
		return "goods/success";
	}
	
	@RequestMapping(value = "delete/{uuid}", method = RequestMethod.GET)
	public String deleteForm(Model uiModel, @PathVariable("uuid") int uuid) {
		Goods goods = goodsService.getByUuid(uuid);
		uiModel.addAttribute("goods", goods);
		return "goods/delete";
	}
	
	@RequestMapping(value = "delete", method = RequestMethod.POST)
	public String delete(@RequestParam("uuid") int uuid) {
		goodsService.delete(uuid);
		return "goods/success";
	}
	
	@RequestMapping(value = "list", method = RequestMethod.GET)
	public String listGoods(@ModelAttribute("goodsWebModel") GoodsWebModel goodsWebModel, Model uiModel) {
			
		GoodsQueryModel goodsQueryModel = null;
		
		String jsonQueryString = goodsWebModel.getJsonQueryString();
		
		if(jsonQueryString == null || jsonQueryString.trim().length() == 0) {
			goodsQueryModel = new GoodsQueryModel();
		} else {
			jsonQueryString = jsonQueryString.replace("-", "%");
			goodsQueryModel = (GoodsQueryModel) JsonHelper.str2Object(jsonQueryString, GoodsQueryModel.class);
		}
		
		goodsQueryModel.getPage().setNowPage(goodsWebModel.getNowPage());
		
		if(goodsWebModel.getPageShow() > 0) {
			goodsQueryModel.getPage().setPageShow(goodsWebModel.getPageShow());
		}
		
		Page dbPage = goodsService.getByConditionPage(goodsQueryModel);
		
		uiModel.addAttribute("goodsWebModel", goodsWebModel);
		uiModel.addAttribute("page", dbPage);
		
		return "goods/list";
	}
	
	@RequestMapping(value = "query", method = RequestMethod.GET)
	public String queryForm() {
		return "goods/query";
	}
	
}
